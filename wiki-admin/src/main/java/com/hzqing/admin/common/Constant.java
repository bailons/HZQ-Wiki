package com.hzqing.admin.common;

/**
 * @author hzqing
 * @date 2019-05-21 15:49
 */
public class Constant {
    private Constant(){

    }
    /**
     * 超级管理员账号
     */
    public static String ADMIN_USER_NAME = "admin";
}
